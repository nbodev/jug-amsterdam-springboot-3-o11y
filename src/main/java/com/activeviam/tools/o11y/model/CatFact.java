package com.activeviam.tools.o11y.model;

import lombok.Data;

@Data
public class CatFact {
	private String fact;
	private int length;
}
