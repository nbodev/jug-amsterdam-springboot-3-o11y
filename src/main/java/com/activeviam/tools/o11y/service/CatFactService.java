package com.activeviam.tools.o11y.service;

import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.activeviam.tools.o11y.model.CatFact;

import io.opentelemetry.api.GlobalOpenTelemetry;
import io.opentelemetry.api.metrics.LongCounter;
import io.opentelemetry.api.metrics.Meter;
import io.opentelemetry.instrumentation.annotations.WithSpan;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CatFactService {

	private final RestTemplate restTemplate;

	@Value("${cat-fact.url:https://catfact.ninja}")
	private String serviceUrl;

	// metrics
	private final Meter meter = GlobalOpenTelemetry.meterBuilder("io.opentelemetry.metrics.catfact").build();
	private LongCounter cumulatedCatFactLength;
	private final AtomicInteger lastCatFactLength = new AtomicInteger(0);

	public CatFactService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@PostConstruct
	private void createMetrics() {
		// counter
		cumulatedCatFactLength = meter
				.counterBuilder("cumulated.cat.fact.length")
				.setDescription("Sum of the received cat fact length over time")
				.setUnit("int")
				.build();

		// gauge
		meter
				.gaugeBuilder("last.cat.fact.length")
				.setDescription("Last received cat fact length")
				.setUnit("int")
				.buildWithCallback(
						r -> r.record(lastCatFactLength.get()));

		// heap
		meter
				.gaugeBuilder("heap.utilization.cat.fact.app")
				.setDescription("Current heap")
				.setUnit("byte")
				.buildWithCallback(r -> r.record(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()));

	}

	@WithSpan
	public CatFact getFact() {
		log.info("Asking for a fact");
		final var getFactUrl = serviceUrl + "/fact";

		try {
			final var catFact = restTemplate.getForObject(getFactUrl, CatFact.class);

			// update the metrics up to date
			updateMetrics(catFact);

			// log the fact
			logCatFact(catFact);
			return catFact;
		} catch (final RestClientException e) {
			log.error("Issue while calling the url [{}]", getFactUrl, e);
			return null;
		}
	}

	@WithSpan
	private void logCatFact(CatFact fact) {
		log.info("Received fact {}.", fact);
	}

	@WithSpan
	private void updateMetrics(CatFact catFact) {
		log.info("Updating the metrics.");

		final var length = catFact != null ? catFact.getLength() : 0;
		// accumulate
		cumulatedCatFactLength.add(length);
		// set current value
		lastCatFactLength.getAndSet(length);
	}

}
