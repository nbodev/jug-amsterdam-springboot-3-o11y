package com.activeviam.tools.o11y.controller;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.activeviam.tools.o11y.model.CatFact;
import com.activeviam.tools.o11y.service.CatFactService;

import io.opentelemetry.instrumentation.annotations.WithSpan;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class ObservabilityController {

	private final CatFactService catFactService;

	public ObservabilityController(CatFactService catFactService) {
		this.catFactService = catFactService;
	}

	@WithSpan
	@GetMapping("/catfact")
	public CatFact getCatFact() {
		log.info("Calling getCatFact");
		return Optional.ofNullable(catFactService.getFact())
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "No fact available for now."));
	}

}
