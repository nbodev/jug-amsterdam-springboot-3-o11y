## Traces

### Configuration

I'm using the OpenTelemetry java agent, it could be seen as sufficient since instrumentation is happening out of the box. Traces and spans will be sent to the collector.<br>
However, we want to see sometimes some specific calls to some of our code methods as spans.<br>
In order to achieve that I used the `@WithSpan` annotation on the methods that I wanted to see in the spans.<br>
As you can see in the `CatFactService` class, I annotated some of the methods:

```
	@WithSpan
	public CatFact getFact() {
			...

			// update the metrics up to date
			updateMetrics(catFact);
			...
			// log the fact
			logCatFact(catFact);
			...
	}

	@WithSpan
	private void logCatFact(CatFact fact) {
		...
	}

	@WithSpan
	private void updateMetrics(CatFact catFact) {
		...
	}
```

When I trigger one fact, I should see those spans.

### How to observe

The SpringBoot app and the local lab should be both running.<br>
Trigger one fact, run  `curl -X GET http://localhost:8888/catfact`, or paste that url `http://localhost:8888/catfact` in your browser. You can also use postman.<br>

I triggered the fact twice, I can read this in the logs:

```
2024-01-11 16:42:41.255 CET INFO 10211 [be97a487c94f5850fd9b1575d7e5486e,cf56d8182600d22e] [http-nio-8888-exec-1] [com.activeviam.tools.o11y.service.CatFactService]: Asking for a fact 
2024-01-11 16:42:41.969 CET INFO 10925 [be97a487c94f5850fd9b1575d7e5486e,a8f9c28a2c598754] [http-nio-8888-exec-1] [com.activeviam.tools.o11y.service.CatFactService]: Updating the metrics. 
2024-01-11 16:42:41.970 CET INFO 10926 [be97a487c94f5850fd9b1575d7e5486e,2dfb045b23edb6e4] [http-nio-8888-exec-1] [com.activeviam.tools.o11y.service.CatFactService]: Received fact CatFact(fact=The cat's tail is used to maintain balance., length=43). 
2024-01-11 16:42:42.717 CET INFO 11673 [c9f0598dd637564a1a51f42f5fefc62d,3fd62eea4a6d78a6] [http-nio-8888-exec-2] [com.activeviam.tools.o11y.service.CatFactService]: Asking for a fact 
2024-01-11 16:42:42.846 CET INFO 11802 [c9f0598dd637564a1a51f42f5fefc62d,3cad6fd9428fe37e] [http-nio-8888-exec-2] [com.activeviam.tools.o11y.service.CatFactService]: Updating the metrics. 
2024-01-11 16:42:42.846 CET INFO 11802 [c9f0598dd637564a1a51f42f5fefc62d,9224c4356157daa2] [http-nio-8888-exec-2] [com.activeviam.tools.o11y.service.CatFactService]: Received fact CatFact(fact=Abraham Lincoln loved cats. He had four of them while he lived in the White House., length=82). 
```

There is an important part in the logs, see below `[%X{trace_id:-},%X{span_id:-}]`:

```
2024-01-11 16:42:41.255 CET INFO 10211 [be97a487c94f5850fd9b1575d7e5486e,cf56d8182600d22e] ...
```

As you can see, I triggered the fact twice, that's why we have two traces `be97a487c94f5850fd9b1575d7e5486e` and `c9f0598dd637564a1a51f42f5fefc62d`, for each trace you have 3 spans appearing in the logs. Those have different spanId but they belong to the same trace. 

![alt text](./img/trace_span.png)


##### Zipkin

Go to `http://localhost:9411/zipkin/` and click on `RUN QUERY` button, you will see the two traces:

![alt text](./img/zipkin_0.png)

Click on the `SHOW` button of one of the traces:

![alt text](./img/zipkin_1.png)


##### Tempo

Go to `http://localhost:3000/`, then click on the explore icon and choose Tempo in the drill down menu:<br>

Make sure you select the right `Service Name`, this is the one we used in `-Dotel.resource.attributes=service.name=o11y-sample-app,service.version=1.0`:


![alt text](./img/tempo_0.png)

As you can see, we can see the spans related to the trace: 

![alt text](./img/tempo_1.png)


### Links

- [https://opentelemetry.io/docs/instrumentation/java/manual/#traces](https://opentelemetry.io/docs/instrumentation/java/manual/#traces)
- [https://opentelemetry.io/docs/instrumentation/java/automatic/annotations/](https://opentelemetry.io/docs/instrumentation/java/automatic/annotations/)