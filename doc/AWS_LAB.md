## The AWS lab

![alt text](../o11y-stack/o11y-aws/img/o11y_aws_schema.png)

### Run

We assume that you know how to use Docker and that Docker is installed on your machine, see [https://www.docker.com/products/docker-desktop/](https://www.docker.com/products/docker-desktop/).<br>
Run `docker-compose up -d` from the folder `o11y-stack/o11y-aws`. This will start the whole environment in the schema above.<br>
We also assume that you have an AWS account and that you did the configuration and credential file settings [https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html).


### Configuration

The `docker-compose.yaml` located under `o11y-stack/o11y-aws` is the file having the configuration of the collector.<br>
There is an important part related to the AWS environment:

```
version: '3.8'

   
services:

  collector:
    image: public.ecr.aws/aws-observability/aws-otel-collector:latest
    container_name: collector
    hostname: collector
    command: ["--config=/etc/collector-config.yaml"]
    environment:
       - AWS_SDK_LOAD_CONFIG=true
       - AWS_CONFIG_FILE=root/.aws/credentials
    volumes:
      - ./collector-config-aws.yaml:/etc/collector-config.yaml
      - ~/.aws:/root/.aws
    ports:
      - "4317:4317" # OTLP gRPC receiver   
```

As mentioned previously, we assume that you have an AWS account and that you did the configuration and credential file settings. This allows you to share the AWS credentials with the AWS collector.<br>
If there is an issue accessing the AWS credentials, you will see this error in the collector logs:

```
collector  | 2024-01-14T10:14:11.702Z	error	awsemfexporter@v0.88.0/emf_exporter.go:138	Error force flushing logs. Skipping to next logPusher.	{"kind": "exporter", "data_type": "metrics", "name": "awsemf", "error": "NoCredentialProviders: no valid providers in chain. Deprecated.\n\tFor verbose messaging see aws.Config.CredentialsChainVerboseErrors"}
```

![alt text](./img/aws-otel-collector.png)

Below the explanation of the collector configuration that you can find here `o11y-stack/o11y-aws/collector-config-aws.yaml`.<br>
We are using the same receivers for the traces, logs and metrics same as the local collector.<br>
We are using different exporters, we are doing the minimum configuration for each one:
- For the metrics AWS EMF Exporter (awsemf).
- For the traces AWS X-Ray Exporter (awsxray).
- For the logs AWS CloudWatch Logs Exporter (awscloudwatchlogs).

This configuration is designed to export metrics, traces and logs from an application to AWS services. The AWS EMF (Embedded Metric Format) exporter is used for metrics, AWS X-Ray exporter for X-Ray data, and AWS CloudWatch Logs exporter for application logs. The specified regions and log group/stream names indicate where the exported data will be stored within the AWS ecosystem.<br>

Let's see how observability looks like with the AWS ecosystem:

### Traces

The traces in X-Ray:
![alt text](./img/aws_xray_0.png)

The details for one trace:
![alt text](./img/aws_xray_1.png)

### Metrics

Note: I did not find the right way to display a counter in the CloudWatch Metrics, for now I'm displaying the gauge:

![alt text](./img/aws_metrics_0.png)

### Logs

The logs:

![alt text](./img/aws_logs_0.png)
      
### Links

- [https://aws.amazon.com/otel/](https://aws.amazon.com/otel/)
- [https://aws-observability.github.io/aws-otel-collector/](https://aws-observability.github.io/aws-otel-collector/)
- [https://aws-otel.github.io/docs/introduction](https://aws-otel.github.io/docs/introduction)