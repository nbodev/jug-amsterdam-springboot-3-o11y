## Metrics

### Configuration

I defined in the `CatFactService` class the metrics, one is a counter and one is a gauge.<br>
A counter, on the other hand, is a monotonically increasing metric. It only goes up and is used to measure cumulative quantities over time.<br>
A gauge represents a single numerical value that can go up or down over time. It measures the current state of a particular aspect of your system.<br>

```

// metrics
	private final Meter meter = GlobalOpenTelemetry.meterBuilder("io.opentelemetry.metrics.catfact").build();
	private LongCounter cumulatedCatFactLength;
	private final AtomicInteger lastCatFactLength = new AtomicInteger(0);

...
@PostConstruct
private void createMetrics() {
// counter
cumulatedCatFactLength = meter
		.counterBuilder("cumulated.cat.fact.length")
		.setDescription("Sum of the received cat fact length over time")
		.setUnit("int")
		.build();

// gauge
meter
		.gaugeBuilder("last.cat.fact.length")
		.setDescription("Last received cat fact length")
		.setUnit("int")
		.buildWithCallback(
				r -> r.record(lastCatFactLength.get()));
...
}				
```

Each time a fact is called, I update the metrics:

```
	@WithSpan
	private void updateMetrics(CatFact catFact) {
		log.info("Updating the metrics.");

		final var length = catFact != null ? catFact.getLength() : 0;
		// accumulate
		cumulatedCatFactLength.add(length);
		// set current value
		lastCatFactLength.getAndSet(length);
	}
```

### How to observe

You can see below how we can observe the metrics in Prometheus, got to `http://localhost:9090/graph` and search for the metrics:
![alt text](./img/prometheus_0.png)

You can also use grafana `http://localhost:3000`, click on Explore, then choose Prometheus, below how I built two widgets showing the metrics defined above:

![alt text](./img/prometheus_1.png)

![alt text](./img/prometheus_2.png)

![alt text](./img/prometheus_3.png)

### Links

- [https://opentelemetry.io/docs/instrumentation/java/manual/#metrics](https://opentelemetry.io/docs/instrumentation/java/manual/#metrics)
- [https://opentelemetry.io/docs/specs/otel/metrics/api/](https://opentelemetry.io/docs/specs/otel/metrics/api/)
