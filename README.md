<p align="center">
  <img width="80" src="./activeviam.svg" />
</p>
<h1 align="center">OpenTelemetry & SpringBoot 3 show room</h1>

### Introduction

A simple SpringBoot 3 project with java 17 showcasing OpenTelemetry.<br>

### The application

#### Build it

Run `mvn clean package` in order to build the project.

#### Run it

Run the `ObservabilityApplication` class,<br>
add the following jvm arguments, we will explain those in the lab section, make sure you set the right path for the `opentelemetry-javaagent.jar` below:

```
-Dotel.traces.exporter=otlp
-Dotel.metrics.exporter=otlp
-Dotel.logs.exporter=otlp
-Dotel.exporter.otlp.endpoint=http://localhost:4317
-Dotel.traces.sampler=always_on
-Dotel.metric.export.interval=1000
-Dotel.resource.attributes=service.name=o11y-sample-app,service.version=1.0
-javaagent:<absolute_path_to>/scripts/o11y-app/opentelemetry-javaagent.jar
```

You can achieve the same thing outside of your IDE by running the script `scripts/o11y-app/start.sh`. From a command window, go to that folder then run the script from there.<br>

You will notice that there are some warnings, this is normal since we did not start the OpenTelemetry lab yet:

```
otel.javaagent 2024-01-11 10:22:53:754 +0100] [OkHttp http://localhost:4317/...] WARN io.opentelemetry.exporter.internal.grpc.GrpcExporter - Failed to export metrics. Server responded with gRPC status code 2. Error message: Failed to connect to localhost/127.0.0.1:4317
```

#### Description

We do have a simple SpringBoot application, having an endpoint `/catfact` defined in `ObservabilityController`.<br>
This endpoint calls `CatFactService#getFact()`.<br>
This call performs and outside call to this url `https://catfact.ninja/fact`, you will get a random fact about the cats and the length of this fact, below an example:

```
{
  "fact": "The domestic cat is the only species able to hold its tail vertically while walking. You can also learn about your cat's present state of mind by observing the posture of his tail.",
  "length": 180
}
```
##### Run the cat facts generator locally

If you want to run the cat facts generator locally and not rely on the `https://catfact.ninja` url, run the project from the folder `scripts/cat-facts`, run the script `start_cat_facts.sh` in a command window, the cat facts generator will start on port `9999`. <br>
Make sure you change the `application.yml`:

```
cat-fact:
  url: https://catfact.ninja
```

Change it to:

```
cat-fact:
  url: http://localhost:9999
```
### Trigger a fact

Assuming you have `curl` installed, run  `curl -X GET http://localhost:8888/catfact`, or paste that url `http://localhost:8888/catfact` in your browser. You can also use postman.<br>

### The local lab

Start the local lab:

[Local lab](./doc/LOCAL_LAB.md)

### The AWS lab

We switch to the AWS lab without changing the code, this time we start an AWS collector.

[AWS lab](./doc/AWS_LAB.md)

### Troubleshooting

Reading the collector console logshelped me a lot, check also the OpenTelemtry issues on their github [https://github.com/open-telemetry](https://github.com/open-telemetry).
