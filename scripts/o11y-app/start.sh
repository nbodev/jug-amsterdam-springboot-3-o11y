#!/bin/bash

REMOTE_JAR=https://github.com/open-telemetry/opentelemetry-java-instrumentation/releases/latest/download/opentelemetry-javaagent.jar
AGENT_FILE=opentelemetry-javaagent.jar

if [ ! -f "${AGENT_FILE}" ]; then
  curl -L ${REMOTE_JAR} --output ${AGENT_FILE}
fi


# for troubleshooting
#export OTEL_TRACES_EXPORTER=logging
#export OTEL_METRICS_EXPORTER=logging
#export OTEL_LOGS_EXPORTER=logging

export OTEL_TRACES_EXPORTER=otlp
export OTEL_METRICS_EXPORTER=otlp
export OTEL_LOGS_EXPORTER=otlp

export OTEL_EXPORTER_OTLP_ENDPOINT=http://localhost:4317 # default endpoint, use the port defined for gRPC
export OTEL_TRACES_SAMPLER=always_on # collects all traces regardless of the sampling rate
export OTEL_METRIC_EXPORT_INTERVAL=1000
export OTEL_RESOURCE_ATTRIBUTES=service.name=o11y-sample-app,service.version=1.0

export JAVA_OPTS="$JAVA_OPTS -Djava.net.preferIPv4Stack=true"

java ${JAVA_OPTS} -javaagent:./${AGENT_FILE} -jar ../../target/o11y-sample-0.0.1-SNAPSHOT.jar